import base64
import Image

## open an image file (.bmp,.jpg,.png,.gif) you have in the working folder
#imageFile = "image.jpg"
#im1 = Image.open(imageFile)
## adjust width and height to your needs
#width = 500
#height = 420
## use one of these filter options to resize the image
#im2 = im1.resize((width, height), Image.NEAREST) # use nearest neighbour
#im3 = im1.resize((width, height), Image.BILINEAR) # linear interpolation in a 2x2 environment
#im4 = im1.resize((width, height), Image.BICUBIC) # cubic spline interpolation in a 4x4 environment
#im5 = im1.resize((width, height), Image.ANTIALIAS) # best down-sizing filter
#ext = ".jpg"
#im2.save("NEAREST" + ext)
#im3.save("BILINEAR" + ext)
#im4.save("BICUBIC" + ext)
#im5.save("ANTIALIAS" + ext)

# Load this source file and strip the header.
#initial_data = open('./image.jpg', 'rb').read()
#
#encoded_data = base64.b64encode(initial_data)
#
#
#f = open('./value.html', 'a')
#f.write('<html><body><img src="data:image/jpeg;base64,')
#f.write(encoded_data)
#f.write('"/></body></html>')
import json

init_d = open('./ANTIALIAS.jpg', 'rb').read()

enc_d = base64.b64encode(init_d)

jsonD = '{"data": "%s" }' % enc_d


data = json.loads(jsonD)

#b = open('./thumb.html', 'a')
#b.write('<html><body><img src="data:image/jpeg;base64,')
#b.write(enc_d)
#b.write('"/></body></html>')

#print encoded_data
#for i in xrange((len(encoded_data)/40)+1):
#    f.write(encoded_data[i*40:(i+1)*40])


